<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/proses','ValidateController@proses');

//baru
Route::get('/univmaju',function (){
  return view('dosen/index');
});
Route::get('/register',function (){
  return view('register');
})->middleware('guest')->name('register');
// Route::post('/register/prosestambah','UnivController@prosestambah');
Route::post('/prosestambah','UnivController@prosestambah');
// Route::get('/index','UnivController@index');
Route::get('/login',function(){
  return view('login');
})->name('login');
Route::post('/dashboard','UnivController@login');
Route::get('/dashboard',function(){
  return view('dosen/index');
})->name('dashboard');

Route::get('/logout','UnivController@logout')->name('logout');

Route::get('/dosen/index','UnivController@tampilData');
