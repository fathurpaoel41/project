<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Register</title>

  <!-- Custom fonts for this template-->
  <link href="{{ url('assets/dosen/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="{{ url('assets/dosen/css/sb-admin.css') }}" rel="stylesheet">

</head>

<body class="bg-dark">

  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Register Data</div>
      <div class= "card-body">
        <form action="/prosestambah" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputNim" class="form-control {{ $errors->has('nim') ? 'is-invalid' : '' }}" placeholder="NIM" name="nim" value="{{old('nim')}}">
              <label for="inputNim">NIM</label>
            </div>
            @if($errors->has('nim'))
              <div class="text-danger">
                <li>{{$errors->first('nim')}}</li>
              </div>
            @endif
          </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputKTP" class="form-control {{ $errors->has('no_ktp') ? 'is-invalid' : '' }}" placeholder="No KTP" name="no_ktp" value="{{old('no_ktp')}}">
                <label for="inputKTP">No KTP</label>
              </div>
          </div>
          @if($errors->has('no_ktp'))
            <div class="text-danger">
              <li>{{$errors->first('no_ktp')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputNama" class="form-control {{ $errors->has('nama') ? 'is-invalid' : '' }}" placeholder="Nama" name="nama" value="{{old('nama')}}">
              <label for="inputNama">Nama</label>
            </div>
          </div>
          @if($errors->has('nama'))
            <div class="text-danger">
              <li>{{$errors->first('nama')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="email" id="inputEmail" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Email address" name="email" value="{{old('email')}}">
              <label for="inputEmail">Email address</label>
            </div>
          </div>
          @if($errors->has('email'))
            <div class="text-danger">
              <li>{{$errors->first('email')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputTempatLahir" class="form-control {{ $errors->has('tempat_lahir') ? 'is-invalid' : '' }}" placeholder="Tempat Lahir" name="tempat_lahir" value="{{old('tempat_lahir')}}">
              <label for="inputTempatLahir">Tempat Lahir</label>
            </div>
          </div>
          @if($errors->has('tempat_lahir'))
            <div class="text-danger">
              <li>{{$errors->first('tempat_lahir')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="date" id="inputDate" class="form-control {{ $errors->has('ttl') ? 'is-invalid' : '' }}" placeholder="Tanggal Lahir" name="ttl" value="{{old('ttl')}}">
              <label for="inputDate">TTL</label>
            </div>
          </div>
          @if($errors->has('ttl'))
            <div class="text-danger">
              <li>{{$errors->first('ttl')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputNoTelpPribadi" class="form-control {{ $errors->has('no_telp_pribadi') ? 'is-invalid' : '' }}" placeholder="No Telp" name="no_telp_pribadi" value="{{old('no_telp_pribadi')}}">
              <label for="inputNoTelpPribadi">No Telp</label>
            </div>
          </div>
          @if($errors->has('no_telp_pribadi'))
            <div class="text-danger">
              <li>{{$errors->first('no_telp_pribadi')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputNamaAyah" class="form-control {{ $errors->has('nama_ayah') ? 'is-invalid' : '' }}" placeholder="Nama Ayah" name="nama_ayah" value="{{old('nama_ayah')}}">
              <label for="inputNamaAyah">Nama Ayah</label>
            </div>
          </div>
          @if($errors->has('nama_ayah'))
            <div class="text-danger">
              <li>{{$errors->first('nama_ayah')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputNamaIbu" class="form-control {{ $errors->has('nama_ibu') ? 'is-invalid' : '' }}" placeholder="Nama Ibu"  name="nama_ibu" value="{{old('nama_ibu')}}">
              <label for="inputNamaIbu">Nama Ibu</label>
            </div>
          </div>
          @if($errors->has('nama_ibu'))
            <div class="text-danger">
              <li>{{$errors->first('nama_ibu')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputNamaWali" class="form-control {{ $errors->has('nama_wali') ? 'is-invalid' : '' }}" placeholder="Nama Wali" name="nama_wali" value="{{old('nama_wali')}}">
              <label for="inputNamaWali">Nama Wali (Boleh Kosong)</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputNoTelpOrtu" class="form-control {{ $errors->has('no_telp_ortu') ? 'is-invalid' : '' }}" placeholder="No Telp Ortu" name="no_telp_ortu" value="{{old('no_telp_ortu')}}">
              <label for="inputNoTelpOrtu">No Telp Ortu</label>
            </div>
          </div>
          @if($errors->has('no_telp_ortu'))
            <div class="text-danger">
              <li>{{$errors->first('no_telp_ortu')}}</li>
            </div>
          @endif
          <div class="form-group">
            <div class="form-label-group">
              <select class="form-control" name="jurusan" placeholder="Jurusan" value="{{old('jurusan')}}">
                <option disabled>--Jurusan--</option>
                <option value="Teknik Informatika">Teknik Informatika</option>
                <option value="Sistem Informasi">Sistem Informasi</option>
                <option value="Ilmu Komputer">Ilmu Komputer</option>
                <option value="Ilmu Komunikasi">Ilmu Komunikasi</option>
              </select>
              {{-- <label for="SelectJurusan">Jurusan</label> --}}
            </div>
          </div>
          <div class="form-group">
              <div class="form-label-group">
                <textarea class="form-control {{ $errors->has('alamat') ? 'is-invalid' : '' }}" name="alamat" id="inputAlamat" cols="82" rows="3" placeholder="Alamat">{{old('alamat')}}</textarea>
              </div>
          </div>
          @if($errors->has('alamat'))
            <div class="text-danger">
              <li>{{$errors->first('alamat')}}</li>
            </div>
          @endif
          <div class="input-group">
            <div class="input-group-prepend {{ $errors->has('foto') ? 'is-invalid' : '' }}">
              <span class="input-group-text" id="inputGroupFileAddon01">Foto</span>
            </div>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="inputGroupFile01"
                aria-describedby="inputGroupFileAddon01" name="foto">
              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
          </div>
          <br />
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="inputUsername" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" placeholder="Username" name="username" value="{{old('username')}}">
              <label for="inputUsername">Username</label>
            </div>
            @if($errors->has('username'))
              <div class="text-danger">
                <li>{{$errors->first('username')}}</li>
              </div>
            @endif
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password" name="password">
                  <label for="inputPassword">Password</label>
                </div>
              </div>
              @if($errors->has('password'))
                <div class="text-danger">
                  <li>{{$errors->first('password')}}</li>
                </div>
              @endif
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="password" id="confirmPassword" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" placeholder="Confirm password" name="password_confirmation">
                  <label for="confirmPassword">Confirm password</label>
                </div>
              </div>
            </div>
            @if($errors->has('confirmPassword'))
              <div class="text-danger">
                <li>{{$errors->first('confirmPassword')}}</li>
              </div>
            @endif
          <br />
          <div class="form-label-group">
            <input type="text" id="inputregpass" class="form-control {{ $errors->has('reg_pass') ? 'is-invalid' : '' }}" placeholder="RegPass" name="reg_pass">
            <label for="inputregpass">Siapa Nama Ibu Kandungmu? (Untuk Lupa Password)</label>
          </div>
          @if($errors->has('reg_pass'))
            <div class="text-danger">
              <li>{{$errors->first('reg_pass')}}</li>
            </div>
            @endif
          </div>
          <br>
          <br>
          <input type="submit" class="btn btn-primary btn-block" value="Next">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="/login"><center>Login Page</center></a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ url('assets/dosen/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('assets/dosen/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ url('assets/dosen/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

</body>

</html>
