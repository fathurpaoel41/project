<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Pegawai;


class UnivController extends Controller
{

  public function prosestambah(Request $kirim)
  {
    $messages = [
      "required" => "Data Harus Diisi",
      "min" => "Minimal 10 karakter",
      "max" => "maksimal 10 karakter",
      "unique" => "Sudah digunakan",
      "confirmed" => "password yang dimasukan tidak sesuai"
    ];

    $validatedData = $kirim->validate([
     'nim' => 'required|min:10|max:10|unique:tb_akun_mhs',
     'no_ktp' => 'required',
     'nama' => 'required',
     'email' => 'required',
     'tempat_lahir' => 'required',
     'ttl' => 'required',
     'no_telp_pribadi' => 'required',
     'nama_ayah' => 'required',
     'nama_ibu' => 'required',
     'no_telp_ortu' => 'required',
     'jurusan' => 'required',
     'alamat' => 'required',
     'username' => 'required|unique:tb_akun_mhs',
     'password' => 'required|min:6|confirmed'
  ],$messages);

    if($validatedData)
    {
      DB::table('tb_data_mhs')->insert([
      'nim' => $kirim->nim,
      'no_ktp' => $kirim->no_ktp,
      'nama' => $kirim->nama,
      'email' => $kirim->email,
      'tempat_lahir' => $kirim->tempat_lahir,
      'ttl' => $kirim->ttl,
      'no_telp_pribadi' => $kirim->no_telp_pribadi,
      'nama_ayah' => $kirim->nama_ayah,
      'nama_ibu' => $kirim->nama_ibu,
      'nama_wali' => $kirim->nama_wali,
      'no_telp_ortu' => $kirim->no_telp_ortu,
      'jurusan' => $kirim->jurusan,
      'alamat' => $kirim->alamat,
      'foto' => $kirim->foto
    ]);

      DB::table('tb_akun_mhs')->insert([
      'nim' => $kirim->nim,
      'username' => $kirim->username,
      'password' => bcrypt($kirim->password)
      ]);

      return view ('/login');

  }else{
    echo "<script>
            alert('Data gagal Ditambahkan');
          </script>";
        }
      }
// tutup class
}
