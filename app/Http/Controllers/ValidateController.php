<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Valudator;
use App\Http\Requests;

class ValidateController extends Controller
{
    public function proses(Request $request)
    {

      $messages = [
        'required' => ':Attribute wajib diisi',
        'min' => ':udah lupain ajh',
        'max' => ':attribute maksimal 20 karakter'
      ];

      $this->validate($request,[
        'nama' => 'required|min:10|max:20',
        'pekerjaan' => 'required',
        'usia' => 'required|numeric'
      ],$messages);

      $data = [
        'data' => $request
      ];

      return view('proses',$data);
    }
}
