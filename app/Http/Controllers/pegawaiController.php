<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Pegawai;


class PegawaiController extends Controller
{
    public function index()
    {
        // eloquent
        $pegawai = Pegawai::paginate(15);
        return view('index',['pegawai'=>$pegawai]);
    }

    public function cari(Request $request)
    {
      $cari = $request->cari;
      // $pegawai = DB::table('pegawai')
      // ->where('pegawai_nama','like',"%".$cari."%")
      // ->paginate();
      $pegawai = Pegawai::where('pegawai_nama','like','%'.$cari.'%')->paginate();
      return view('index',["pegawai"=>$pegawai]);
    }

    public function tambah()
    {
      return view('tambah');
    }

    public function prosestambah(Request $kirim)
    {
      $messages = [
        "required" => "Data Harus Diisi",
        "min" => "Minimal 3 karakter",
        "max" => "maksimal 50 karakter"
      ];

      $validatedData = $kirim->validate([
       'nama' => 'required|min:3',
       'jabatan' => 'required',
       'umur' => 'required',
       'alamat' => 'required'
    ],$messages);

    if($validatedData){
      Pegawai::create([
        'pegawai_nama' => $kirim->nama,
        'pegawai_jabatan' => $kirim->jabatan,
        'pegawai_umur' => $kirim->umur,
        'pegawai_alamat' => $kirim->alamat,
      ]);

      echo "<script>
              alert('Data Berhasil Ditambahkan');
              document.location.href= '/pegawai';
            </script>";
    }else{
      echo "<script>
              alert('Data gagal Ditambahkan');
              document.location.href='/pegawai';
            </script>";
          }
    }

    public function update(Request $request)
    {
      $messages = [
        'required' => 'Data Harus Diisi',
        'min' => 'Data Minimal 3 Karakter',
        'max' => 'data maksimal 25 karakter',
        'between' => 'Umur 1-9'
      ];

      $validate = $request->validate([
        'nama' => 'required|min:3|max:25',
        'jabatan' => 'required',
        'umur' => 'required',
        'alamat' => 'required'
      ],$messages);

      if($validate)
      {
        DB::table('pegawai')->where('pegawai_id', $request->pegawai_id)->update([
          'pegawai_nama' => $request->nama,
          'pegawai_jabatan' => $request->jabatan,
          'pegawai_umur' => $request->umur,
          'pegawai_alamat' => $request->alamat
        ]);
        // $pegawai = Pegawai::find($id);
        // $pegawai->nama = $request->nama;
        // $pegawai->jabatan = $request->jabatan;
        // $pegawai->umur = $request->umur;
        // $pegawai->alamat = $request->alamat;
        // $pegawai->save();
        echo "<script>
                alert('Data Berhasil Diubah');
                document.location.href= '/pegawai';
              </script>";
      }else {
        echo "<script>
                alert('Data gagal Ditambahkan');
                document.location.href='/mahasiswa';
              </script>";
      }
    }

    public function edit($pegawai_id)
    {
      $pegawai = DB::table('pegawai')->where('pegawai_id', $pegawai_id)->get();
      return view('edit',['pegawai'=>$pegawai]);

      // $pegawai = Pegawai::find($pegawai_id);
      // return view('edit',['pegawai'=>$pegawai]);
    }

    public function hapus($pegawai_id)
    {
      if($pegawai_id)
      {
        DB::table('pegawai')->where('pegawai_id', $pegawai_id)->delete();
        echo "<script>
                alert('Data Berhasil dihapus');
                document.location.href= '/pegawai';
              </script>";

        // $pegawai = Pegawai::find($pegawai_id);
        // $pegawai->delete();
      }else{
        echo "<script>
                alert('Data gagal Ditambahkan');
                document.location.href='/mahasiswa';
              </script>";
      }
    }

// tutup class
}
