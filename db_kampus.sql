-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Feb 2020 pada 16.35
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kampus`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `matkul_pel`
--

CREATE TABLE `matkul_pel` (
  `kode_matkul` varchar(10) NOT NULL,
  `pertemuan` varchar(5) NOT NULL,
  `judul_pel` varchar(50) NOT NULL,
  `isi_pel` varchar(9999) NOT NULL,
  `klik` int(10) NOT NULL,
  `file` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2020_01_30_134533_users', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_akun_dosen`
--

CREATE TABLE `tb_akun_dosen` (
  `kode_dosen` varchar(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_hint` varchar(20) NOT NULL,
  `reg_pass` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_dosen`
--

CREATE TABLE `tb_data_dosen` (
  `kode_dosen` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `ttl` date NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_matkul`
--

CREATE TABLE `tb_data_matkul` (
  `kode_matkul` varchar(10) NOT NULL,
  `matkul` varchar(20) NOT NULL,
  `sks` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_mhs`
--

CREATE TABLE `tb_data_mhs` (
  `nim` int(10) NOT NULL,
  `no_ktp` int(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `ttl` date NOT NULL,
  `no_telp_pribadi` varchar(15) NOT NULL,
  `nama_ayah` varchar(20) NOT NULL,
  `nama_ibu` varchar(20) NOT NULL,
  `nama_wali` varchar(20) DEFAULT NULL,
  `no_telp_ortu` varchar(15) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_data_mhs`
--

INSERT INTO `tb_data_mhs` (`nim`, `no_ktp`, `nama`, `email`, `tempat_lahir`, `ttl`, `no_telp_pribadi`, `nama_ayah`, `nama_ibu`, `nama_wali`, `no_telp_ortu`, `jurusan`, `alamat`, `foto`) VALUES
(1911511123, 123123, 'asdfsd', '234@gmail.co', 'bogr', '0009-09-09', '243', 'asda', 'dada', NULL, 'dadsasd', 'Teknik Informatika', 'bogor', NULL),
(1911511412, 123123, 'asdfsd', '234@gmail.co', 'bogr', '0009-09-09', '243', 'asda', 'dada', NULL, 'dadsasd', 'Teknik Informatika', 'bogor', NULL),
(1911511434, 123132, 'fathur', 'yuyu@gmail.com', 'bogor', '2002-09-05', '4234', 'sda', 'sad', NULL, '123123', 'Teknik Informatika', 'bogor', NULL),
(1911511440, 123123123, 'Muhammad Fathurachman', 'fathurpaoel41@gmail.com', 'Bogor', '2002-05-09', '088709626279', 'Ayah', 'Mamah', NULL, '0812312432', 'Teknik Informatika', 'Bogor', NULL),
(1911511442, 21313123, 'Farah', 'fathurpaoel41@gmail.com', 'Bogor', '2002-09-05', '0887090626279', 'Ayah', 'Ibu', NULL, '08423423', 'Teknik Informatika', 'Bogor', NULL),
(1911511445, 21313123, 'Farah', 'fathurpaoel41@gmail.com', 'Bogor', '2002-09-05', '0887090626279', 'Ayah', 'Ibu', NULL, '08423423', 'Teknik Informatika', 'Bogor', NULL),
(1911511447, 21313123, 'Farah', 'fathurpaoel41@gmail.com', 'Bogor', '2002-09-05', '0887090626279', 'Ayah', 'Ibu', NULL, '08423423', 'Teknik Informatika', 'Bogor', NULL),
(1911511449, 123132, 'fathur', 'yuyu@gmail.com', 'bogor', '2002-09-05', '4234', 'sda', 'sad', NULL, '123123', 'Teknik Informatika', 'bogor', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_diskusi`
--

CREATE TABLE `tb_diskusi` (
  `kode_matkul` varchar(10) NOT NULL,
  `pertemuan` int(5) NOT NULL,
  `isi_komentar` varchar(100) NOT NULL,
  `nim` int(10) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `suka` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_konten`
--

CREATE TABLE `tb_konten` (
  `kode_konten` varchar(100) NOT NULL,
  `nama_konten` varchar(50) NOT NULL,
  `klik` int(100) NOT NULL,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_matkul`
--

CREATE TABLE `tb_matkul` (
  `kode_matkul` varchar(10) NOT NULL,
  `kode_dosen` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_nilai`
--

CREATE TABLE `tb_nilai` (
  `kode_matkul` varchar(10) NOT NULL,
  `nim` int(10) NOT NULL,
  `nilai` int(100) NOT NULL,
  `grade` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_soal`
--

CREATE TABLE `tb_soal` (
  `kode_matkul` varchar(10) NOT NULL,
  `no_soal` int(10) NOT NULL,
  `isi_soal` varchar(9999) NOT NULL,
  `gambar_1` varchar(50) DEFAULT NULL,
  `gambar_2` varchar(50) DEFAULT NULL,
  `gambar_3` varchar(50) DEFAULT NULL,
  `kunci_jawaban` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `nim` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(999) NOT NULL,
  `reg_pass` varchar(999) DEFAULT NULL,
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`nim`, `username`, `password`, `reg_pass`, `created_at`, `updated_at`) VALUES
(1911511123, 'fathur76', '$2y$10$riHZ0mANoG6erz3aoS645elNVF0O/8AykdnbrFoGIpgHdgAJgXzN2', NULL, '2020-02-02 15:25:17', '2020-02-02 15:25:17'),
(1911511434, 'fathur0987', '$2y$10$Fsb8M7IdGVLJ9I5IeVzuIO3afru1.4KLcoRY9qF10xoUrpStINwh.', NULL, '2020-02-02 15:17:45', '2020-02-02 15:17:45'),
(1911511440, 'fathurpaoel41', '$2y$10$19MmwX/FOHRniRrjNL7kle1oxzRy6RxEbjy1fxIFO9yOhQCZL6q0K', 'Ibu', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `matkul_pel`
--
ALTER TABLE `matkul_pel`
  ADD KEY `kode_matkul` (`kode_matkul`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_akun_dosen`
--
ALTER TABLE `tb_akun_dosen`
  ADD KEY `id_dosen` (`kode_dosen`);

--
-- Indeks untuk tabel `tb_data_dosen`
--
ALTER TABLE `tb_data_dosen`
  ADD PRIMARY KEY (`kode_dosen`),
  ADD KEY `kode_dosen` (`kode_dosen`);

--
-- Indeks untuk tabel `tb_data_matkul`
--
ALTER TABLE `tb_data_matkul`
  ADD PRIMARY KEY (`kode_matkul`),
  ADD KEY `kode_matkul` (`kode_matkul`);

--
-- Indeks untuk tabel `tb_data_mhs`
--
ALTER TABLE `tb_data_mhs`
  ADD PRIMARY KEY (`nim`);

--
-- Indeks untuk tabel `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  ADD KEY `kode_matkul` (`kode_matkul`);

--
-- Indeks untuk tabel `tb_konten`
--
ALTER TABLE `tb_konten`
  ADD PRIMARY KEY (`kode_konten`);

--
-- Indeks untuk tabel `tb_matkul`
--
ALTER TABLE `tb_matkul`
  ADD UNIQUE KEY `kode_matkul` (`kode_matkul`,`kode_dosen`),
  ADD KEY `kode_dosen` (`kode_dosen`);

--
-- Indeks untuk tabel `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD KEY `kode_matkul` (`kode_matkul`),
  ADD KEY `nim` (`nim`);

--
-- Indeks untuk tabel `tb_soal`
--
ALTER TABLE `tb_soal`
  ADD KEY `kode_matkul` (`kode_matkul`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `users_nim_unique` (`nim`),
  ADD KEY `nim` (`nim`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_data_dosen`
--
ALTER TABLE `tb_data_dosen`
  ADD CONSTRAINT `tb_data_dosen_ibfk_1` FOREIGN KEY (`kode_dosen`) REFERENCES `tb_akun_dosen` (`kode_dosen`);

--
-- Ketidakleluasaan untuk tabel `tb_data_matkul`
--
ALTER TABLE `tb_data_matkul`
  ADD CONSTRAINT `tb_data_matkul_ibfk_1` FOREIGN KEY (`kode_matkul`) REFERENCES `tb_matkul` (`kode_matkul`),
  ADD CONSTRAINT `tb_data_matkul_ibfk_2` FOREIGN KEY (`kode_matkul`) REFERENCES `matkul_pel` (`kode_matkul`),
  ADD CONSTRAINT `tb_data_matkul_ibfk_3` FOREIGN KEY (`kode_matkul`) REFERENCES `tb_soal` (`kode_matkul`),
  ADD CONSTRAINT `tb_data_matkul_ibfk_4` FOREIGN KEY (`kode_matkul`) REFERENCES `tb_nilai` (`kode_matkul`),
  ADD CONSTRAINT `tb_data_matkul_ibfk_5` FOREIGN KEY (`kode_matkul`) REFERENCES `tb_diskusi` (`kode_matkul`);

--
-- Ketidakleluasaan untuk tabel `tb_matkul`
--
ALTER TABLE `tb_matkul`
  ADD CONSTRAINT `tb_matkul_ibfk_1` FOREIGN KEY (`kode_dosen`) REFERENCES `tb_data_dosen` (`kode_dosen`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `tb_data_mhs` (`nim`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
